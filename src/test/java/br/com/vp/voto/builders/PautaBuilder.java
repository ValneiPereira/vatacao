package br.com.vp.voto.builders;

import br.com.vp.voto.api.request.PautaRequest;
import br.com.vp.voto.model.document.PautaDocument;
import br.com.vp.voto.model.dto.PautaDTO;

import java.util.List;

public class PautaBuilder {

    public static PautaDocument pauta() {
        return PautaDocument.builder()
            .id(1L)
            .titulo("tituloTeste")
            .descricao("descricaoTeste")
            .build();
    }

    public static PautaDTO pautaDTO() {
        return PautaDTO.builder()
            .id(1L)
            .titulo("tituloTeste")
            .descricao("descricaoTeste")
            .build();
    }

    public static PautaRequest request() {
        return PautaRequest.builder()
            .titulo("tituloTeste")
            .descricao("descricaoTeste")
            .build();
    }

    public static List<PautaDocument> listPautaMock() {
        return List.of(pauta(), pauta());
    }
}
