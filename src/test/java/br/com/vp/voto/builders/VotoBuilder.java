package br.com.vp.voto.builders;

import br.com.vp.voto.model.document.VotoDocument;
import br.com.vp.voto.model.dto.VotoInDTO;
import br.com.vp.voto.model.dto.VotoOutDTO;
import br.com.vp.voto.model.enums.VotoEnum;

import java.util.List;

public class VotoBuilder {

    public static VotoDocument voto() {
        return VotoDocument.builder()
            .id(1L)
            .voto(VotoEnum.SIM)
            .idSessao(1L)
            .sessao(SessaoBuilder.sessao())
            .idAssociado(1L)
            .associado(AssociadoBuilder.associado())
            .build();
    }

    public static VotoInDTO votoInDTO() {
        return VotoInDTO.builder()
            .id(1L)
            .idAssociado(1L)
            .idSessao(1L)
            .voto(VotoEnum.SIM)
            .build();
    }

    public static VotoOutDTO votoOutDto() {
        return VotoOutDTO.builder()
            .voto(VotoEnum.SIM)
            .associado(AssociadoBuilder.associadoDTO())
            .build();
    }

    public static VotoDocument votoSimplificado() {
        return VotoDocument.builder()
            .id(1L)
            .voto(VotoEnum.SIM)
            .idSessao(1L)
            .idAssociado(1L)
            .build();
    }


    public static List<VotoDocument> listVoto() {
        return List.of(voto(), voto());
    }

    public static List<VotoDocument> listVotoSimplificado() {
        return List.of(votoSimplificado(), votoSimplificado());
    }

    public static List<VotoOutDTO> listVotoOutDTO() {
        return List.of(votoOutDto(), votoOutDto());
    }
}
