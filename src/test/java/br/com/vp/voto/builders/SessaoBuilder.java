package br.com.vp.voto.builders;

import br.com.vp.voto.model.document.SessaoDocument;
import br.com.vp.voto.model.dto.SessaoComVotosDTO;
import br.com.vp.voto.model.dto.SessaoInDTO;
import br.com.vp.voto.model.dto.SessaoOutDTO;
import br.com.vp.voto.model.enums.StatusSessaoEnum;

import java.time.LocalDateTime;
import java.util.List;

public class SessaoBuilder {

    public static SessaoDocument sessao() {
        return SessaoDocument.builder()
            .id(1L)
            .duracao(null)
            .status(StatusSessaoEnum.ABERTA)
            .dataInicio(LocalDateTime.now())
            .dataFim(LocalDateTime.now().plusMinutes(1))
            .idPauta(1L)
            .pauta(PautaBuilder.pauta())
            .votos(VotoBuilder.listVotoSimplificado())
            .build();
    }

    public static SessaoInDTO sessaoInDTO() {
        return SessaoInDTO.builder()
            .duracao(null)
            .idPauta(1L)
            .build();
    }

    public static SessaoOutDTO sessaoOutDTO() {
        return SessaoOutDTO.builder()
            .id(1L)
            .duracao(null)
            .dataInicio(LocalDateTime.now())
            .dataFim(LocalDateTime.now().plusMinutes(1))
            .build();
    }

    public static SessaoComVotosDTO sessaoComVotosDTO() {
        return SessaoComVotosDTO.builder()
            .id(1L)
            .duracao(null)
            .dataInicio(LocalDateTime.now())
            .dataFim(LocalDateTime.now().plusMinutes(1))
            .votos(VotoBuilder.listVotoOutDTO())
            .build();
    }

    public static List<SessaoDocument> listSessao() {
        return List.of(sessao(), sessao());
    }

}
