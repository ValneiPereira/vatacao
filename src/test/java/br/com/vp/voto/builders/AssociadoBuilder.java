package br.com.vp.voto.builders;

import br.com.vp.voto.api.request.AssociadoRequest;
import br.com.vp.voto.model.document.AssociadoDocument;
import br.com.vp.voto.model.dto.AssociadoDTO;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.List;


@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class AssociadoBuilder {

    public static AssociadoDocument associado() {
        return AssociadoDocument.builder()
            .id(1L)
            .nome("Augusto")
            .cpf("44772811702")
            .build();
    }

    public static AssociadoDTO associadoDTO() {
        return AssociadoDTO.builder()
            .id(1L)
            .nome("Augusto")
            .cpf("44772811702")
            .build();
    }

    public static AssociadoRequest associadoRequest() {
        return AssociadoRequest.builder()
            .nome("Augusto")
            .cpf("44772811702")
            .build();
    }

    public static List<AssociadoDocument> listAssociadoMock() {
        return List.of(associado(), associado());
    }
}
