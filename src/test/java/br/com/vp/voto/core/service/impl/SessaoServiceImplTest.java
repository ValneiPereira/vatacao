package br.com.vp.voto.core.service.impl;

import br.com.vp.voto.builders.SessaoBuilder;
import br.com.vp.voto.core.repository.SessaoRepository;
import br.com.vp.voto.model.document.SessaoDocument;
import br.com.vp.voto.model.enums.StatusSessaoEnum;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class SessaoServiceImplTest {

    @InjectMocks
    private SessaoServiceImpl service;

    @Mock
    private SessaoRepository repository;

    private SessaoDocument document;
    private List<SessaoDocument> documentList;

    @BeforeEach
    void setUp() {
        document = SessaoBuilder.sessao();
        documentList = SessaoBuilder.listSessao();
    }

    @Test
    void deveCriarUmaNovaSessao() {

        when(this.repository.save(any(SessaoDocument.class))).thenReturn(document);
        SessaoDocument sessao = service.novo(document);

        assertNotNull(sessao);
        assertEquals(1L, sessao.getId());
    }

    @Test
    void deveBuscarTodos() {

        when(repository.findAll()).thenReturn(documentList);
        List<SessaoDocument> sessoes = service.buscarTodos();

        assertNotNull(sessoes);
        assertEquals(2, sessoes.size());
    }

    @Test
    void buscarTodasComStatusAberta() {

        when(repository.findAllByStatus(any(StatusSessaoEnum.class))).thenReturn(documentList);
        List<SessaoDocument> abertas = this.service.buscarTodasComStatusAberta();

        assertNotNull(abertas);
        assertEquals(2, abertas.stream().filter(sessao -> StatusSessaoEnum.ABERTA.equals(sessao.getStatus())).count());
    }

    @Test
    void deveBuscarUmaSessaovalida() {

        when(this.repository.findById(anyLong())).thenReturn(Optional.of(document));

        SessaoDocument sessao = service.buscar(1L);

        assertNotNull(sessao);
        assertEquals(1L, sessao.getId());
    }
}
