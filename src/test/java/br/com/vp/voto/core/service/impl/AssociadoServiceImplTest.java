package br.com.vp.voto.core.service.impl;

import br.com.vp.voto.api.exceptions.CpfCadastradoException;
import br.com.vp.voto.api.exceptions.NaoEncontradoException;
import br.com.vp.voto.api.request.AssociadoRequest;
import br.com.vp.voto.builders.AssociadoBuilder;
import br.com.vp.voto.core.converter.AssociadoConverter;
import br.com.vp.voto.core.repository.AssociadoRepository;
import br.com.vp.voto.core.service.SequenceGeneratorService;
import br.com.vp.voto.model.document.AssociadoDocument;
import br.com.vp.voto.model.dto.AssociadoDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class AssociadoServiceImplTest {

    @InjectMocks
    private AssociadoServiceImpl service;

    @Mock
    private AssociadoRepository repository;

    @Mock
    private AssociadoConverter associadoConverter;

    @Mock
    private SequenceGeneratorService generatorService;

    private AssociadoDocument document;
    private AssociadoDTO dto;
    private AssociadoRequest request;

    @BeforeEach
    void setUp(){
        document = AssociadoBuilder.associado();
        dto = AssociadoBuilder.associadoDTO();
        request = AssociadoBuilder.associadoRequest();
    }

    @Test
    @DisplayName("Cria um novo associado válido.")
    void deveSalvarNovoAssociado() {
        when(associadoConverter.fromRequest(request)).thenReturn(document);
        when(repository.save(any(AssociadoDocument.class))).thenReturn(document);
        when(generatorService.generateSequence(AssociadoDocument.SEQUENCE_NAME)).thenReturn(1L);
        when(associadoConverter.fromDTO(document)).thenReturn(dto);

        AssociadoDTO novoAssociado = service.salvar(request);
        assertEquals(novoAssociado.getId(), document.getId());
        verify(repository).save(document);
    }

    @Test
    @DisplayName("Retorna uma lista com todos os associados.")
    void deveBuscarTodos() {
        List<AssociadoDocument> mockList = AssociadoBuilder.listAssociadoMock();

        when(repository.findAll()).thenReturn(mockList);
        List<AssociadoDTO> associados = service.buscarTodos();
        assertNotNull(associados);
        assertEquals(2, associados.size());
    }

    @Test
    @DisplayName("Busca um associado")
    void deveBuscarAssociado() {
        when(associadoConverter.fromRequest(request)).thenReturn(document);
        when(repository.findById(anyLong())).thenReturn(Optional.ofNullable(document));
        when(associadoConverter.fromDTO(document)).thenReturn(dto);

        AssociadoDTO associado = service.buscar(1L);

        assertNotNull(associado);
        assertEquals(1L, associado.getId());
    }

    @Test
    @DisplayName("Joga uma exceção, existe associado para o id informado.")
    void deveBuscarAssociadoComIdInvalido() {
        when(repository.findById(anyLong())).thenThrow(NaoEncontradoException.class);

        assertThrows(NaoEncontradoException.class, () -> service.buscar(5L));
    }

    @Test
    @DisplayName("Edita um associado.")
    void deveEditarAssociado() {

        when(associadoConverter.fromEntity(dto)).thenReturn(document);
        when(repository.findById(anyLong())).thenReturn(Optional.of(document));
        when(repository.save(any(AssociadoDocument.class))).thenReturn(document);
        when(associadoConverter.fromDTO(document)).thenReturn(dto);

        AssociadoDTO editado = service.editar(1L, request);
        assertNotNull(editado);
        assertEquals("44772811702", editado.getCpf());
    }

    @Test
    @DisplayName("Exclui um associado")
    void deveExcluir() {
        doNothing().when(repository).deleteById(anyLong());
        service.excluir(1L);
        assertThrows(NaoEncontradoException.class, () -> service.buscar(1L));
    }

    @Test
    @DisplayName("LancaCpfCadastradoException")
    void deveLancarCpfCadastradoException(){
        when(associadoConverter.fromRequest(request)).thenReturn(document);
        when(repository.findAllByCpf(anyString())).thenReturn(Collections.singletonList(document));
        when(repository.save(any(AssociadoDocument.class))).thenReturn(document);

        assertThrows(CpfCadastradoException.class, () -> service.salvar(request));
    }
}
