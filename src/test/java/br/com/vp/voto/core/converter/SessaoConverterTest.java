package br.com.vp.voto.core.converter;

import br.com.vp.voto.builders.PautaBuilder;
import br.com.vp.voto.builders.SessaoBuilder;
import br.com.vp.voto.builders.VotoBuilder;
import br.com.vp.voto.model.document.PautaDocument;
import br.com.vp.voto.model.document.SessaoDocument;
import br.com.vp.voto.model.dto.PautaDTO;
import br.com.vp.voto.model.dto.SessaoComVotosDTO;
import br.com.vp.voto.model.dto.SessaoInDTO;
import br.com.vp.voto.model.dto.SessaoOutDTO;
import br.com.vp.voto.model.dto.VotoOutDTO;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class SessaoConverterTest {

    @InjectMocks
    private SessaoConverter sessaoConverter;

    @Mock
    private VotoConverter votoConverter;

    @Mock
    private PautaConverter pautaConverter;

    @Test
    void deveConverterFromDTO() {
        SessaoDocument sessaoDocument = SessaoBuilder.sessao();
        PautaDTO pautaDTO = PautaBuilder.pautaDTO();

        when(pautaConverter.fromDTO(any(PautaDocument.class))).thenReturn(pautaDTO);
        SessaoOutDTO sessaoOutDTO = sessaoConverter.fromDTO(sessaoDocument);

        assertNotNull(sessaoOutDTO);
        assertEquals(1L, sessaoOutDTO.getId());
    }

    @Test
    void deveConverterFromEntity() {
        SessaoInDTO sessaoInDTO = SessaoBuilder.sessaoInDTO();

        SessaoDocument sessaoDocument = sessaoConverter.fromEntity(sessaoInDTO);

        assertNotNull(sessaoDocument);
        assertEquals(1L, sessaoDocument.getIdPauta());

    }


    @Test
    void deveConverterUmasessaoToSessaoComVotos() {

        PautaDTO pautaDTO = PautaBuilder.pautaDTO();
        List<VotoOutDTO> outMockList = VotoBuilder.listVotoOutDTO();
        SessaoDocument sessaoDocument = SessaoBuilder.sessao();

        when(this.pautaConverter.fromDTO(any(PautaDocument.class))).thenReturn(pautaDTO);
        when(this.votoConverter.listEntityToListDTO(anyList())).thenReturn(outMockList);

        SessaoComVotosDTO sessaoComVotosDTO = sessaoConverter.sessaoToSessaoComVotos(sessaoDocument);

        assertNotNull(sessaoComVotosDTO);
        assertEquals(1L, sessaoComVotosDTO.getId());
    }
}
