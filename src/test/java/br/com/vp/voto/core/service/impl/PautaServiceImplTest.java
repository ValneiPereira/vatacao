package br.com.vp.voto.core.service.impl;

import br.com.vp.voto.api.exceptions.NaoEncontradoException;
import br.com.vp.voto.api.request.PautaRequest;
import br.com.vp.voto.builders.PautaBuilder;
import br.com.vp.voto.core.converter.PautaConverter;
import br.com.vp.voto.core.repository.PautaRepository;
import br.com.vp.voto.core.service.SequenceGeneratorService;
import br.com.vp.voto.model.document.PautaDocument;
import br.com.vp.voto.model.dto.PautaDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
@ExtendWith(SpringExtension.class)
class PautaServiceImplTest {

    @InjectMocks
    private PautaServiceImpl service;

    @Mock
    private PautaRepository repository;

    @Mock
    private PautaConverter converter;

    @Mock
    private SequenceGeneratorService generatorService;

    private PautaDTO dto;
    private PautaDocument document;
    private PautaRequest request;

    @BeforeEach
    void setUp(){
        dto = PautaBuilder.pautaDTO();
        document = PautaBuilder.pauta();
        request = PautaBuilder.request();
    }

    @Test
    @DisplayName("Cria uma nova pauta.")
    void deveSalvarUmaPauta() {

        when(converter.fromRequest(request)).thenReturn(document);
        when(repository.save(any(PautaDocument.class))).thenReturn(document);
        when(generatorService.generateSequence(PautaDocument.SEQUENCE_NAME)).thenReturn(1L);
        when(converter.fromDTO(document)).thenReturn(dto);

        PautaDTO pautaDTO = service.salvar(request);
        assertNotNull(pautaDTO);
        assertEquals(1L, pautaDTO.getId());
    }

    @Test
    @DisplayName("Retorna uma Lista com todas as pautas.")
    void deveBuscarTodasAsPautas() {
        List<PautaDocument> mockList = PautaBuilder.listPautaMock();

        when(this.repository.findAll()).thenReturn(mockList);
        List<PautaDTO> pautas = service.buscarTodos();

        assertNotNull(pautas);
        assertEquals(2, pautas.size());
    }

    @Test
    @DisplayName("Busca uma pauta com um id existente.")
    void deveBuscarPautaComIdValido() {
        when(converter.fromEntity(dto)).thenReturn(document);
        when(this.repository.findById(anyLong())).thenReturn(Optional.ofNullable(document));
        when(converter.fromDTO(document)).thenReturn(dto);

        PautaDTO pauta = service.buscar(1L);

        assertNotNull(pauta);
        assertEquals(1L, pauta.getId());
    }

    @Test
    @DisplayName("Edita uma pauta.")
    void deveEditarUmaPauta() {


        when(converter.fromEntity(dto)).thenReturn(document);
        when(this.repository.findById(anyLong())).thenReturn(Optional.of(document));
        when(this.repository.save(any(PautaDocument.class))).thenReturn(document);
        when(converter.fromDTO(document)).thenReturn(dto);

        PautaDTO editado = service.editar(1L, request);

        assertNotNull(editado);
        assertEquals("descricaoTeste", editado.getDescricao());
    }

    @Test
    @DisplayName("Exclui uma pauta.")
    void deveExcluirUmaPauta() {
        doNothing().when(this.repository).deleteById(anyLong());

        service.excluir(1L);

        assertThrows(NaoEncontradoException.class, () -> service.buscar(1L));
    }
}
