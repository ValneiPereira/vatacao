package br.com.vp.voto.core.business.impl;

import br.com.vp.voto.builders.PautaBuilder;
import br.com.vp.voto.builders.SessaoBuilder;
import br.com.vp.voto.builders.VotoBuilder;
import br.com.vp.voto.core.converter.PautaConverter;
import br.com.vp.voto.core.converter.SessaoConverter;
import br.com.vp.voto.core.service.SequenceGeneratorService;
import br.com.vp.voto.core.service.impl.PautaServiceImpl;
import br.com.vp.voto.core.service.impl.SessaoServiceImpl;
import br.com.vp.voto.core.service.impl.VotoServiceImpl;
import br.com.vp.voto.message.VoteResultsProducer;
import br.com.vp.voto.model.document.AssociadoDocument;
import br.com.vp.voto.model.document.SessaoDocument;
import br.com.vp.voto.model.document.VotoDocument;
import br.com.vp.voto.model.dto.ResultadoVotacaoDTO;
import br.com.vp.voto.model.dto.SessaoComVotosDTO;
import br.com.vp.voto.model.dto.SessaoInDTO;
import br.com.vp.voto.model.dto.SessaoOutDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class SessaoBusinessImplTest {

    @InjectMocks
    private SessaoBusinessImpl business;

    @Mock
    private SessaoServiceImpl sessaoService;

    @Mock
    private VotoServiceImpl votoService;

    @Mock
    private PautaServiceImpl pautaService;

    @Mock
    private VoteResultsProducer producer;

    @Mock
    private SessaoConverter sessaoConverter;

    @Mock
    private SequenceGeneratorService generatorService;

    @Mock
    private PautaConverter pautaConverter;

    private SessaoDocument sessao;
    private SessaoInDTO sessaoInDTO;
    private SessaoOutDTO sessaoOutDTO;
    private SessaoComVotosDTO comVotosDTO;

    @BeforeEach
    void setUp(){
        sessao= SessaoBuilder.sessao();
        sessaoInDTO = SessaoBuilder.sessaoInDTO();
        sessaoOutDTO = SessaoBuilder.sessaoOutDTO();
        comVotosDTO = SessaoBuilder.sessaoComVotosDTO();
    }

    @Test
    void deveCriarumanovaSessao() {

        when(pautaConverter.fromEntity(any())).thenReturn(PautaBuilder.pauta());
        when(sessaoConverter.fromEntity(sessaoInDTO)).thenReturn(sessao);
        when(sessaoService.novo(any(SessaoDocument.class))).thenReturn(sessao);
        when(generatorService.generateSequence(AssociadoDocument.SEQUENCE_NAME)).thenReturn(1L);
        when(sessaoConverter.fromDTO(sessao)).thenReturn(sessaoOutDTO);

        SessaoOutDTO novo = business.novo(sessaoInDTO);

        assertNotNull(novo);
        assertEquals(1L, novo.getId());
    }

    @Test
    void deveBuscarTodasSessoes() {
        List<SessaoDocument> listSessao = SessaoBuilder.listSessao();

        when(sessaoService.buscarTodos()).thenReturn(listSessao);

        List<SessaoOutDTO> sessaoOutDTOS = business.buscarTodasSessoes();

        assertNotNull(sessaoOutDTOS);
        assertEquals(2, sessaoOutDTOS.size());
    }

    @Test
    void devebuscarUmaSessao() {

        when(sessaoConverter.fromDTO(sessao)).thenReturn(sessaoOutDTO);
        business.buscarSessao(1L);

        assertNotNull(sessao);
        assertEquals(1L, sessao.getId());
    }

    @Test
    void deveDefinirVotosDaSessao() {

        List<VotoDocument> votoList = VotoBuilder.listVotoSimplificado();

        when(sessaoService.buscar(anyLong())).thenReturn(sessao);
        when(votoService.buscarTodosPorIdSessao(anyLong())).thenReturn(votoList);
        when(sessaoConverter.sessaoToSessaoComVotos(sessao)).thenReturn(comVotosDTO);

        SessaoComVotosDTO sessaoComVotosDTO = business.definirVotosDaSessao(1L);

        assertNotNull(sessaoComVotosDTO);
        assertEquals(2, sessaoComVotosDTO.getVotos().size());
    }

    @Test
    void deveContabilizarVotos() {
        doNothing().when(producer).sendMessage(anyLong(), any(ResultadoVotacaoDTO.class));

        business.contabilizarVotos(1L);
        assertNotNull(producer);
    }
}
