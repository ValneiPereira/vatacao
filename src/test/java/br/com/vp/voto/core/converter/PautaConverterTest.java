package br.com.vp.voto.core.converter;

import br.com.vp.voto.api.request.PautaRequest;
import br.com.vp.voto.builders.PautaBuilder;
import br.com.vp.voto.model.document.PautaDocument;
import br.com.vp.voto.model.dto.PautaDTO;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;
@ExtendWith(SpringExtension.class)
class PautaConverterTest {

    @InjectMocks
    private PautaConverter converter;

    @Test
    void deveConverterFromEntity() {
        PautaDTO dto = PautaBuilder.pautaDTO();
        PautaDocument pautaDocument = converter.fromEntity(dto);

        assertNotNull(pautaDocument);
        assertEquals(1L, pautaDocument.getId());
    }

    @Test
    void deveConverterFromDTO() {
        PautaDocument document = PautaBuilder.pauta();
        PautaDTO dto = converter.fromDTO(document);

        assertNotNull(dto);
        assertEquals(1L, dto.getId());
    }

    @Test
    void deveConverterFromRequest() {
        PautaRequest  request = PautaBuilder.request();
        PautaDocument pautaDocument = converter.fromRequest(request);

        assertNotNull(pautaDocument);
        assertEquals("tituloTeste", pautaDocument.getTitulo());
    }
}
