package br.com.vp.voto.core.converter;

import br.com.vp.voto.api.request.AssociadoRequest;
import br.com.vp.voto.builders.AssociadoBuilder;
import br.com.vp.voto.model.document.AssociadoDocument;
import br.com.vp.voto.model.dto.AssociadoDTO;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;
@ExtendWith(SpringExtension.class)
class AssociadoConverterTest {

    @InjectMocks
    private AssociadoConverter converter;

    @Test
    void deveConverterParaDTO() {
        AssociadoDocument entityMock = AssociadoBuilder.associado();

        AssociadoDTO dto = converter.fromDTO(entityMock);

        assertNotNull(dto);
        assertEquals(1L, dto.getId());
    }

    @Test
    void deveConverterParaEntidade() {
        AssociadoDTO dto = AssociadoBuilder.associadoDTO();

        AssociadoDocument entity = converter.fromEntity(dto);

        assertNotNull(entity);
        assertEquals(1L, entity.getId());
    }

    @Test
    void deveConverterParaRequest() {
        AssociadoRequest request = AssociadoBuilder.associadoRequest();

        AssociadoDocument entity = converter.fromRequest(request);

        assertNotNull(entity);
        assertEquals("Augusto", entity.getNome());
    }
}
