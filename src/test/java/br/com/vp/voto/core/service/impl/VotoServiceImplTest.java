package br.com.vp.voto.core.service.impl;

import br.com.vp.voto.builders.VotoBuilder;
import br.com.vp.voto.core.repository.VotoRepository;
import br.com.vp.voto.model.document.VotoDocument;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class VotoServiceImplTest {

    @InjectMocks
    private VotoServiceImpl service;

    @Mock
    private VotoRepository repository;

    private VotoDocument votoDocument;

    private List<VotoDocument> documentList;

    @BeforeEach
    void setUp() {
        votoDocument = VotoBuilder.voto();
        documentList = VotoBuilder.listVoto();
    }

    @Test
    void deveCriarNovoVoto() {
        when(repository.save(any(VotoDocument.class))).thenReturn(votoDocument);

        VotoDocument voto = service.novo(votoDocument);

        assertNotNull(voto);
        assertEquals(1L, voto.getId());
    }

    @Test
    void buscarTodos() {

        when(repository.findAll()).thenReturn(documentList);
        List<VotoDocument> votos = service.buscarTodos();

        assertNotNull(votos);
        assertEquals(2, votos.size());
    }

    @Test
    void deveBuscarUmaSessao() {

        when(repository.findById(anyLong())).thenReturn(Optional.of(votoDocument));
        VotoDocument voto = service.buscar(1L);

        assertNotNull(voto);
        assertEquals(1L, voto.getId());
    }

    @Test
    void buscarTodosPorIdSessao() {

        when(repository.findAllByIdSessao(anyLong())).thenReturn(documentList);
        List<VotoDocument> votos = this.service.buscarTodosPorIdSessao(1L);

        assertNotNull(votos);
        assertEquals(2, votos.size());
    }
}
