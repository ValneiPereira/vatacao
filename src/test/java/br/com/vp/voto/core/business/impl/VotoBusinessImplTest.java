package br.com.vp.voto.core.business.impl;

import br.com.vp.voto.api.exceptions.SessaoEncerradaException;
import br.com.vp.voto.builders.AssociadoBuilder;
import br.com.vp.voto.builders.SessaoBuilder;
import br.com.vp.voto.builders.VotoBuilder;
import br.com.vp.voto.core.converter.AssociadoConverter;
import br.com.vp.voto.core.converter.VotoConverter;
import br.com.vp.voto.core.service.impl.AssociadoServiceImpl;
import br.com.vp.voto.core.service.impl.SessaoServiceImpl;
import br.com.vp.voto.core.service.impl.VotoServiceImpl;
import br.com.vp.voto.model.document.AssociadoDocument;
import br.com.vp.voto.model.document.SessaoDocument;
import br.com.vp.voto.model.document.VotoDocument;
import br.com.vp.voto.model.dto.VotoInDTO;
import br.com.vp.voto.model.dto.VotoOutDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class VotoBusinessImplTest {

    @InjectMocks
    private VotoBusinessImpl business;

    @Mock
    private VotoServiceImpl votoService;

    @Mock
    private SessaoServiceImpl sessaoService;

    @Mock
    private AssociadoServiceImpl associadoService;

    @Mock
    private VotoConverter votoConverter;

    @Mock
    private AssociadoConverter associadoConverter;

    private VotoDocument voto;
    private VotoInDTO votoInDTO;
    private SessaoDocument sessao;
    private AssociadoDocument associado;

    @BeforeEach
    void setUp() {
        voto = VotoBuilder.voto();
        votoInDTO = VotoBuilder.votoInDTO();
        sessao = SessaoBuilder.sessao();
        associado = AssociadoBuilder.associado();
    }

    @Test
    void deveCriarVotoValido() {

        when(votoConverter.fromEntity(any())).thenReturn(voto);
        when(votoService.novo(any(VotoDocument.class))).thenReturn(voto);
        when(sessaoService.buscar(anyLong())).thenReturn(sessao);
        when(associadoService.buscar(anyLong())).thenReturn(AssociadoBuilder.associadoDTO());
        when(associadoConverter.fromEntity(any())).thenReturn(associado);
        when(votoConverter.fromDTO(any())).thenReturn(VotoBuilder.votoOutDto());

        VotoOutDTO novo = business.novo(votoInDTO);

        assertNotNull(novo);
        assertEquals(1L, novo.getAssociado().getId());
    }

    @Test
    void deveMostrarExcecaoAoCriarVotoEmSessaoEncerrada() {

        sessao.setDataFim(LocalDateTime.now().minusMinutes(3));

        when(votoConverter.fromEntity(any())).thenReturn(voto);
        when(sessaoService.buscar(anyLong())).thenReturn(sessao);

        assertThrows(SessaoEncerradaException.class, () -> business.novo(votoInDTO));
    }

    @Test
    void deveBuscarVotosPorSessao() {
        List<VotoDocument> documentList = VotoBuilder.listVotoSimplificado();

        when(votoService.buscarTodosPorIdSessao(anyLong())).thenReturn(documentList);
        List<VotoDocument> votos = this.business.buscarVotosPorSessao(1L);

        assertNotNull(votos);
        assertEquals(2, votos.size());
    }

    @Test
    void deveBuscarTodosVotos() {
        List<VotoDocument> documentList = VotoBuilder.listVoto();

        when(this.votoService.buscarTodos()).thenReturn(documentList);
        List<VotoOutDTO> votos = this.business.buscarTodosVotos();

        assertNotNull(votos);
        assertEquals(2, votos.size());
    }

    @Test
    void deveBuscarVotoPorId() {

        when(votoService.buscar(anyLong())).thenReturn(voto);
        when(votoConverter.fromDTO(any())).thenReturn(VotoBuilder.votoOutDto());

        VotoOutDTO votoOutDTO = business.buscarVoto(1L);

        assertNotNull(votoOutDTO);
        assertEquals(1L, votoOutDTO.getAssociado().getId());
    }
}
