package br.com.vp.voto.model.document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "associado")
public class AssociadoDocument {

    @Transient
    public static final String SEQUENCE_NAME = "associado_sequence";

    @Id
    private Long id;
    private String nome;
    private String cpf;

}
