package br.com.vp.voto.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SessaoComVotosDTO {

    private Long id;
    private Long duracao;
    private LocalDateTime dataInicio;
    private LocalDateTime dataFim;
    private PautaDTO pauta;
    private List<VotoOutDTO> votos;
}
