package br.com.vp.voto.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.br.CPF;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AssociadoDTO {

    private Long id;
    private String nome;
    @CPF(message = "CPF inválido!")
    private String cpf;
}
