package br.com.vp.voto.model.document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.annotation.Id;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "pauta")
public class PautaDocument {

    @Transient
    public static final String SEQUENCE_NAME = "pauta_sequence";

    @Id
    private Long id;
    private String titulo;
    private String descricao;
}
