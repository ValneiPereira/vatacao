package br.com.vp.voto.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SessaoOutDTO {

    private Long id;
    private Long duracao;
    private LocalDateTime dataInicio;
    private LocalDateTime dataFim;
    private PautaDTO pauta;
}
