package br.com.vp.voto.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ResultadoVotacaoDTO {
    private Long votosAFavor;
    private Long votosContra;
    private Integer total;
}
