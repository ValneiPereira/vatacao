package br.com.vp.voto.model.enums;

public enum StatusSessaoEnum {
    ABERTA, FECHADA
}
