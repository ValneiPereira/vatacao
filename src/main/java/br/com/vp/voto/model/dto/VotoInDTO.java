package br.com.vp.voto.model.dto;

import br.com.vp.voto.model.enums.VotoEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class VotoInDTO {

    private Long id;
    private VotoEnum voto;
    private Long idSessao;
    private Long idAssociado;
}
