package br.com.vp.voto.model.document;

import br.com.vp.voto.model.enums.VotoEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "voto")
public class VotoDocument {

    @Transient
    public static final String SEQUENCE_NAME = "voto_sequence";

    @Id
    private Long id;
    private VotoEnum voto;
    private Long idSessao;
    private SessaoDocument sessao;
    private Long idAssociado;
    private AssociadoDocument associado;
}
