package br.com.vp.voto.model.document;

import br.com.vp.voto.model.enums.StatusSessaoEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.annotation.Id;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "sessao")
public class SessaoDocument {

    @Transient
    public static final String SEQUENCE_NAME = "sessao_sequence";

    @Id
    private Long id;
    private Long duracao;
    private StatusSessaoEnum status;
    private LocalDateTime dataInicio;
    private LocalDateTime dataFim;
    private Long idPauta;
    private PautaDocument pauta;
    private List<VotoDocument> votos;
}
