package br.com.vp.voto.core.business.impl;

import br.com.vp.voto.core.business.SessaoBusiness;
import br.com.vp.voto.core.converter.PautaConverter;
import br.com.vp.voto.core.converter.SessaoConverter;
import br.com.vp.voto.core.service.PautaService;
import br.com.vp.voto.core.service.SequenceGeneratorService;
import br.com.vp.voto.core.service.SessaoService;
import br.com.vp.voto.core.service.VotoService;
import br.com.vp.voto.message.VoteResultsProducer;
import br.com.vp.voto.model.document.PautaDocument;
import br.com.vp.voto.model.document.SessaoDocument;
import br.com.vp.voto.model.dto.PautaDTO;
import br.com.vp.voto.model.dto.ResultadoVotacaoDTO;
import br.com.vp.voto.model.dto.SessaoComVotosDTO;
import br.com.vp.voto.model.dto.SessaoInDTO;
import br.com.vp.voto.model.dto.SessaoOutDTO;
import br.com.vp.voto.model.enums.StatusSessaoEnum;
import br.com.vp.voto.model.enums.VotoEnum;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.time.LocalDateTime.*;

@AllArgsConstructor
@Slf4j
@Service
public class SessaoBusinessImpl implements SessaoBusiness {

    private static final Long TEMPO_PADRAO_DE_SESSAO = 1L;

    private final SessaoService sessaoService;
    private final PautaService pautaService;
    private final VotoService votoService;
    private final SessaoConverter sessaoConverter;
    private final PautaConverter  pautaConverter;
    private final SequenceGeneratorService sequenceGeneratorService;
    private final VoteResultsProducer producer;

    @Override
    public SessaoOutDTO novo(SessaoInDTO dto) {
        SessaoDocument sessaoDocument = sessaoConverter.fromEntity(dto);
        PautaDocument pautaDocument = pautaConverter.fromEntity(buscarPauta(sessaoDocument.getIdPauta()));

        sessaoDocument.setId(sequenceGeneratorService.generateSequence(SessaoDocument.SEQUENCE_NAME));
        sessaoDocument.setPauta(pautaDocument);
        sessaoDocument.setDataInicio(now());
        sessaoDocument.setDataFim(LocalDateTime.now().plusMinutes(tempoDaSessao(sessaoDocument.getDuracao())));
        sessaoDocument.setStatus(StatusSessaoEnum.ABERTA);

        sessaoService.novo(sessaoDocument);
        log.info("Nova sessão criada.");

        return sessaoConverter.fromDTO(sessaoDocument);
    }

    @Override
    public List<SessaoOutDTO> buscarTodasSessoes() {
        List<SessaoDocument> sessaoDocuments = sessaoService.buscarTodos();
        return sessaoDocuments.stream().map(sessaoConverter::fromDTO).collect(Collectors.toList());
    }

    @Override
    public SessaoOutDTO buscarSessao(Long id) {
        SessaoDocument sessaoDocument = sessaoService.buscar(id);
        return sessaoConverter.fromDTO(sessaoDocument);
    }

    public SessaoComVotosDTO definirVotosDaSessao(Long idSessao) {
        SessaoDocument sessaoDocument = sessaoService.buscar(idSessao);
        sessaoDocument.setVotos(votoService.buscarTodosPorIdSessao(idSessao));
        return sessaoConverter.sessaoToSessaoComVotos(sessaoDocument);
    }

    public void contabilizarVotos(Long idSessao) {

        producer.sendMessage(idSessao, ResultadoVotacaoDTO.builder()
            .votosAFavor(votoService.buscarTodosPorIdSessao(idSessao).stream()
                .filter(voto -> voto.getVoto().equals(VotoEnum.SIM))
                .count())
            .votosContra(votoService.buscarTodosPorIdSessao(idSessao).stream()
                .filter(voto -> voto.getVoto().equals(VotoEnum.NAO))
                .count())
            .total(votoService.buscarTodosPorIdSessao(idSessao).size())
            .build());
    }

    /**
     * Cada 5sec ele verifica se Sessao esta fechada.
     */
    @Scheduled(fixedRate = 5000)
    private void scheduleVerificarSessao() {
        verificaSeSessaoEstaFechada();
    }

    private void verificaSeSessaoEstaFechada() {

        sessaoService.buscarTodasComStatusAberta().stream()
            .filter(sessao -> sessao.getDataFim().isBefore(LocalDateTime.now()))
            .forEach(sessao -> {
                atualizarStatusSessao(sessao);
                contabilizarVotos(sessao.getId());
            });
    }

    private void atualizarStatusSessao(SessaoDocument sessao) {
        sessao.setStatus(StatusSessaoEnum.FECHADA);
        sessaoService.novo(sessao);
    }

    private PautaDTO buscarPauta(Long idPauta) {
        return pautaService.buscar(idPauta);
    }

    private Long tempoDaSessao(Long tempo) {
        return Optional.ofNullable(tempo).orElse(TEMPO_PADRAO_DE_SESSAO);
    }
}
