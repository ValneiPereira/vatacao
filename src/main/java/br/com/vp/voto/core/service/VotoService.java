package br.com.vp.voto.core.service;

import br.com.vp.voto.model.document.VotoDocument;

import java.util.List;

public interface VotoService {
    VotoDocument novo(VotoDocument voto);
    List<VotoDocument> buscarTodos();
    VotoDocument buscar(Long id);
    List<VotoDocument> buscarTodosPorIdSessao(Long idSessao);
}
