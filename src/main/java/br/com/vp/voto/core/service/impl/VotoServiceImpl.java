package br.com.vp.voto.core.service.impl;

import br.com.vp.voto.api.exceptions.NaoEncontradoException;
import br.com.vp.voto.core.repository.VotoRepository;
import br.com.vp.voto.core.service.VotoService;
import br.com.vp.voto.model.document.VotoDocument;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
@AllArgsConstructor
@Service
public class VotoServiceImpl implements VotoService {

    private final VotoRepository repository;

    @Override
    public VotoDocument novo(VotoDocument voto) {
        return repository.save(voto);
    }

    @Override
    public List<VotoDocument> buscarTodos() {
        return repository.findAll();
    }

    @Override
    public VotoDocument buscar(Long id) {
        return repository.findById(id).orElseThrow(() -> new NaoEncontradoException("Voto não encontrado"));
    }

    @Override
    public List<VotoDocument> buscarTodosPorIdSessao(Long idSessao) {
        return repository.findAllByIdSessao(idSessao);
    }
}
