package br.com.vp.voto.core.repository;

import br.com.vp.voto.model.document.VotoDocument;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VotoRepository extends MongoRepository<VotoDocument, Long> {
    List<VotoDocument> findAllByIdSessao(Long idSessao);
}
