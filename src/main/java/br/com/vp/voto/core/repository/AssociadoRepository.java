package br.com.vp.voto.core.repository;

import br.com.vp.voto.model.document.AssociadoDocument;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AssociadoRepository extends MongoRepository<AssociadoDocument, Long> {
    List<AssociadoDocument> findAllByCpf(String cpf);
}
