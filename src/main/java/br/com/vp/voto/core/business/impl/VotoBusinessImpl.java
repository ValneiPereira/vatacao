package br.com.vp.voto.core.business.impl;

import br.com.vp.voto.api.exceptions.SessaoEncerradaException;
import br.com.vp.voto.api.exceptions.VotoJaEfetuadoException;
import br.com.vp.voto.core.business.VotoBusiness;
import br.com.vp.voto.core.converter.AssociadoConverter;
import br.com.vp.voto.core.converter.VotoConverter;
import br.com.vp.voto.core.service.AssociadoService;
import br.com.vp.voto.core.service.SessaoService;
import br.com.vp.voto.core.service.VotoService;
import br.com.vp.voto.model.document.VotoDocument;
import br.com.vp.voto.model.dto.VotoInDTO;
import br.com.vp.voto.model.dto.VotoOutDTO;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@Slf4j
@Service
public class VotoBusinessImpl implements VotoBusiness {

    private final VotoService votoService;
    private final SessaoService sessaoService;
    private final AssociadoService associadoService;
    private final AssociadoConverter associadoConverter;
    private final VotoConverter votoConverter;

    @Override
    public VotoOutDTO novo(VotoInDTO dto) {
        VotoDocument voto = votoConverter.fromEntity(dto);

        voto.setSessao(sessaoService.buscar(voto.getIdSessao()));
        if (LocalDateTime.now().isAfter(voto.getSessao().getDataFim())) {
            throw new SessaoEncerradaException("A sessão de votação já foi encerrada.");
        }

        validaSeAssociadoJaVotou(voto);
        voto.setAssociado(associadoConverter.fromEntity(associadoService.buscar(dto.getIdAssociado())));
        votoService.novo(voto);
        log.info("Novo voto criado.");
        return votoConverter.fromDTO(voto);
    }

    public List<VotoDocument> buscarVotosPorSessao(Long idSessao) {
        return this.votoService.buscarTodosPorIdSessao(idSessao);
    }

    @Override
    public List<VotoOutDTO> buscarTodosVotos() {
        List<VotoDocument> votos = votoService.buscarTodos();
        return votos.stream().map(votoConverter::fromDTO).collect(Collectors.toList());
    }

    @Override
    public VotoOutDTO buscarVoto(Long id) {
        VotoDocument voto = votoService.buscar(id);
        return votoConverter.fromDTO(voto);
    }

    private void validaSeAssociadoJaVotou(VotoDocument novoVoto) {
        List<VotoDocument> votos = buscarVotosPorSessao(novoVoto.getIdSessao());
        if (!votos.isEmpty() && votos.stream().anyMatch(voto -> novoVoto.getIdAssociado().equals(voto.getIdAssociado()))) {
            throw new VotoJaEfetuadoException("Já votou nesta pauta.");
        }
    }
}
