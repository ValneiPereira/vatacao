package br.com.vp.voto.core.repository;

import br.com.vp.voto.model.document.SessaoDocument;
import br.com.vp.voto.model.enums.StatusSessaoEnum;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SessaoRepository extends MongoRepository<SessaoDocument, Long> {
    List<SessaoDocument> findAllByStatus(StatusSessaoEnum status);
}
