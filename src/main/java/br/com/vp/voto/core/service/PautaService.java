package br.com.vp.voto.core.service;

import br.com.vp.voto.api.request.PautaRequest;
import br.com.vp.voto.model.dto.PautaDTO;

import java.util.List;

public interface PautaService {
    PautaDTO salvar (PautaRequest pauta);
    List<PautaDTO> buscarTodos();
    PautaDTO buscar (Long id);
    PautaDTO editar(Long id, PautaRequest request);
    void excluir(Long id);
}

