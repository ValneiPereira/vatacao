package br.com.vp.voto.core.service.impl;

import br.com.vp.voto.api.exceptions.NaoEncontradoException;
import br.com.vp.voto.api.request.PautaRequest;
import br.com.vp.voto.core.converter.PautaConverter;
import br.com.vp.voto.core.repository.PautaRepository;
import br.com.vp.voto.core.service.PautaService;
import br.com.vp.voto.core.service.SequenceGeneratorService;
import br.com.vp.voto.model.document.PautaDocument;
import br.com.vp.voto.model.dto.PautaDTO;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
@AllArgsConstructor
public class PautaServiceImpl implements PautaService {

    private static final String NAO_ENCONTRADA = "Pauta nao encontrada";
    private final PautaConverter converter;
    private final PautaRepository repository;
    private final SequenceGeneratorService sequenceGeneratorService;


    @Override
    @Transactional
    public PautaDTO salvar(PautaRequest request) {
        PautaDocument pautaDocument = converter.fromRequest(request);
        pautaDocument.setId(sequenceGeneratorService.generateSequence(PautaDocument.SEQUENCE_NAME));

        repository.save(pautaDocument);
        log.info("Nova pauta criada: {}", pautaDocument.getTitulo());
        return converter.fromDTO(pautaDocument);
    }

    @Override
    public List<PautaDTO> buscarTodos() {
        List<PautaDocument> pautaDocuments = repository.findAll();
        return pautaDocuments.stream().map(converter::fromDTO).collect(Collectors.toList());
    }

    @Override
    public PautaDTO buscar(Long id) {
        PautaDocument pautaDocument = repository.findById(id).orElseThrow(()-> new NaoEncontradoException(NAO_ENCONTRADA));
        return converter.fromDTO(pautaDocument);
    }

    @Override
    @Transactional
    public PautaDTO editar(Long id, PautaRequest request){
        PautaDTO pautaAtual = buscar(id);
        PautaDocument pautaDocument = converter.fromEntity(pautaAtual);
        pautaDocument.setTitulo(request.getTitulo());
        pautaDocument.setDescricao(request.getDescricao());

        repository.save(pautaDocument);
        log.info("Alterado dados da pauta com id: {}", pautaDocument.getId());
        return converter.fromDTO(pautaDocument);
    }

    @Override
    public void excluir(Long id) {
        repository.deleteById(id);
        log.info("Pauta excluída. Id: {}.", id);
    }
}
