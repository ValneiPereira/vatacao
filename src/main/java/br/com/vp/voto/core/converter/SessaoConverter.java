package br.com.vp.voto.core.converter;

import br.com.vp.voto.model.document.SessaoDocument;
import br.com.vp.voto.model.dto.SessaoComVotosDTO;
import br.com.vp.voto.model.dto.SessaoInDTO;
import br.com.vp.voto.model.dto.SessaoOutDTO;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@AllArgsConstructor
@Component
public class SessaoConverter {

    private final PautaConverter pautaConverter;
    private final VotoConverter votoConverter;

    public SessaoOutDTO fromDTO(SessaoDocument entity) {
        return SessaoOutDTO.builder()
            .id(entity.getId())
            .duracao(entity.getDuracao())
            .dataInicio(entity.getDataInicio())
            .dataFim(entity.getDataFim())
            .pauta(pautaConverter.fromDTO(entity.getPauta()))
            .build();
    }

    public SessaoDocument fromEntity(SessaoInDTO dto) {
        return SessaoDocument.builder()
            .idPauta(dto.getIdPauta())
            .duracao(dto.getDuracao())
            .build();
    }


    public SessaoComVotosDTO sessaoToSessaoComVotos(SessaoDocument sessao) {
        return SessaoComVotosDTO.builder()
            .id(sessao.getId())
            .dataInicio(sessao.getDataInicio())
            .dataFim(sessao.getDataFim())
            .pauta(pautaConverter.fromDTO(sessao.getPauta()))
            .votos(votoConverter.listEntityToListDTO(sessao.getVotos()))
            .build();
    }
}
