package br.com.vp.voto.core.converter;

import br.com.vp.voto.model.document.VotoDocument;
import br.com.vp.voto.model.dto.VotoInDTO;
import br.com.vp.voto.model.dto.VotoOutDTO;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@Component
public class VotoConverter {
    private final AssociadoConverter associadoConverter;

    public VotoOutDTO fromDTO(VotoDocument entity) {
        return VotoOutDTO.builder()
            .voto(entity.getVoto())
            .associado(associadoConverter.fromDTO(entity.getAssociado()))
            .build();
    }

    public VotoDocument fromEntity(VotoInDTO dto) {
        return VotoDocument.builder()
            .id(dto.getId())
            .voto(dto.getVoto())
            .idSessao(dto.getIdSessao())
            .idAssociado(dto.getIdAssociado())
            .build();
    }

    public List<VotoOutDTO> listEntityToListDTO(List<VotoDocument> entities) {
        return entities.stream().map(this::fromDTO).collect(Collectors.toList());
    }
}
