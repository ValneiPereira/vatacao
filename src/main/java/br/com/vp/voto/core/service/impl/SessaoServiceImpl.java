package br.com.vp.voto.core.service.impl;

import br.com.vp.voto.api.exceptions.NaoEncontradoException;
import br.com.vp.voto.core.repository.SessaoRepository;
import br.com.vp.voto.core.service.SessaoService;
import br.com.vp.voto.model.document.SessaoDocument;
import br.com.vp.voto.model.enums.StatusSessaoEnum;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@AllArgsConstructor
@Service
public class SessaoServiceImpl implements SessaoService {

    private final SessaoRepository repository;

    @Override
    public SessaoDocument novo(SessaoDocument sessao) {
        return repository.save(sessao);
    }

    @Override
    public List<SessaoDocument> buscarTodos() {
        return repository.findAll();
    }

    @Override
    public List<SessaoDocument> buscarTodasComStatusAberta() {
        return repository.findAllByStatus(StatusSessaoEnum.ABERTA);
    }

    @Override
    public SessaoDocument buscar(Long id) {
        return repository.findById(id).orElseThrow(() -> new NaoEncontradoException("Sessão não encontrada."));
    }
}
