package br.com.vp.voto.core.business;

import br.com.vp.voto.model.dto.SessaoComVotosDTO;
import br.com.vp.voto.model.dto.SessaoInDTO;
import br.com.vp.voto.model.dto.SessaoOutDTO;

import java.util.List;

public interface SessaoBusiness {
    SessaoOutDTO novo(SessaoInDTO dto);
    List<SessaoOutDTO> buscarTodasSessoes ();
    SessaoOutDTO buscarSessao(Long id);
    SessaoComVotosDTO definirVotosDaSessao(Long idSessao);


}
