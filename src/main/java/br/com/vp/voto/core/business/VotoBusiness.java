package br.com.vp.voto.core.business;

import br.com.vp.voto.model.dto.VotoInDTO;
import br.com.vp.voto.model.dto.VotoOutDTO;

import java.util.List;

public interface VotoBusiness {
    VotoOutDTO novo(VotoInDTO dto);
    List<VotoOutDTO> buscarTodosVotos();
    VotoOutDTO buscarVoto(Long id);
}
