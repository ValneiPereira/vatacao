package br.com.vp.voto.core.converter;

import br.com.vp.voto.api.request.AssociadoRequest;
import br.com.vp.voto.model.document.AssociadoDocument;
import br.com.vp.voto.model.dto.AssociadoDTO;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class AssociadoConverter {

    public AssociadoDocument fromEntity(AssociadoDTO associadoDTO){
        return AssociadoDocument.builder()
            .id(associadoDTO.getId())
            .nome(associadoDTO.getNome())
            .cpf(associadoDTO.getCpf())
            .build();
    }

    public AssociadoDTO fromDTO(AssociadoDocument associadoDocument){
        return AssociadoDTO.builder()
            .id(associadoDocument.getId())
            .nome(associadoDocument.getNome())
            .cpf(associadoDocument.getCpf())
            .build();
    }

    public AssociadoDocument fromRequest(AssociadoRequest request){
        return AssociadoDocument.builder()
            .nome(request.getNome())
            .cpf(request.getCpf())
            .build();
    }
}
