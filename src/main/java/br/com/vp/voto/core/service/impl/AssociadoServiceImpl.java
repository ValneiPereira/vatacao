package br.com.vp.voto.core.service.impl;

import br.com.vp.voto.api.exceptions.CpfCadastradoException;
import br.com.vp.voto.api.exceptions.NaoEncontradoException;
import br.com.vp.voto.api.request.AssociadoRequest;
import br.com.vp.voto.core.converter.AssociadoConverter;
import br.com.vp.voto.core.repository.AssociadoRepository;
import br.com.vp.voto.core.service.AssociadoService;
import br.com.vp.voto.core.service.SequenceGeneratorService;
import br.com.vp.voto.model.document.AssociadoDocument;
import br.com.vp.voto.model.dto.AssociadoDTO;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
@AllArgsConstructor
public class AssociadoServiceImpl implements AssociadoService {

    private static final String NAO_ENCONTRADO = "Associiado não encontrado";
    private final AssociadoConverter converter;
    private final AssociadoRepository repository;
    private final SequenceGeneratorService sequenceGeneratorService;

    @Override
    @Transactional
    public AssociadoDTO salvar(AssociadoRequest request) {
        AssociadoDocument associadoDocument = converter.fromRequest(request);
        associadoDocument.setId(sequenceGeneratorService.generateSequence(AssociadoDocument.SEQUENCE_NAME));
        verificaCadastroExistente(request.getCpf());

        repository.save(associadoDocument);
        log.info("Novo associado criado: {}", associadoDocument.getNome());
        return converter.fromDTO(associadoDocument);
    }

    @Override
    public List<AssociadoDTO> buscarTodos() {
        List<AssociadoDocument> associadoDocuments = repository.findAll();
        return associadoDocuments.stream().map(converter::fromDTO).collect(Collectors.toList());
    }

    @Override
    public AssociadoDTO buscar(Long id) {
        AssociadoDocument associadoDocument = repository.findById(id).orElseThrow(() ->  new NaoEncontradoException(NAO_ENCONTRADO));
        return converter.fromDTO(associadoDocument);
    }

    @Override
    @Transactional
    public AssociadoDTO editar(Long id, AssociadoRequest request) {
        AssociadoDTO associadoAtual = buscar(id);
        AssociadoDocument associadoDocument = converter.fromEntity(associadoAtual);
        associadoDocument.setNome(request.getNome());
        associadoDocument.setCpf(request.getCpf());

        repository.save(associadoDocument);
        log.info("Alterado dados do associado com id: {}", associadoDocument.getId());
        return converter.fromDTO(associadoDocument);
    }

    @Override
    public void excluir(Long id) {
        repository.deleteById(id);
        log.info("Associado excluído. Id: {}.", id);
    }

    private void verificaCadastroExistente(String cpf){
        List<AssociadoDocument> listCpfs = repository.findAllByCpf(cpf);
        if (!listCpfs.isEmpty() && listCpfs.stream().anyMatch(a -> cpf.equals(a.getCpf()))){
            log.info("CPF já cadastrado. Id:{}.",cpf);
            throw new CpfCadastradoException("Já existe este CPF cadastrado");
        }
    }
}
