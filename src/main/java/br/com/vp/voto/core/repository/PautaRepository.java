package br.com.vp.voto.core.repository;

import br.com.vp.voto.model.document.PautaDocument;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PautaRepository extends MongoRepository<PautaDocument, Long> {
}
