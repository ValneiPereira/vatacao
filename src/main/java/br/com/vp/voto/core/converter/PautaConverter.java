package br.com.vp.voto.core.converter;

import br.com.vp.voto.api.request.PautaRequest;
import br.com.vp.voto.model.document.PautaDocument;
import br.com.vp.voto.model.dto.PautaDTO;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class PautaConverter {

    public PautaDocument fromEntity(PautaDTO dto){
        return PautaDocument.builder()
            .id(dto.getId())
            .titulo(dto.getTitulo())
            .descricao(dto.getDescricao())
            .build();
    }

    public PautaDTO fromDTO(PautaDocument document){
        return PautaDTO.builder()
            .id(document.getId())
            .titulo(document.getTitulo())
            .descricao(document.getDescricao())
            .build();
    }

    public PautaDocument fromRequest(PautaRequest request) {
        return PautaDocument.builder()
            .titulo(request.getTitulo())
            .descricao(request.getDescricao())
            .build();
    }
}
