package br.com.vp.voto.core.service;

import br.com.vp.voto.model.document.SessaoDocument;

import java.util.List;

public interface SessaoService {


    SessaoDocument novo(SessaoDocument sessao);

    List<SessaoDocument> buscarTodos();

    List<SessaoDocument> buscarTodasComStatusAberta();

    SessaoDocument buscar(Long id);
}
