package br.com.vp.voto.core.service;

import br.com.vp.voto.api.request.AssociadoRequest;
import br.com.vp.voto.model.dto.AssociadoDTO;

import java.util.List;

public interface AssociadoService {
    AssociadoDTO salvar(AssociadoRequest dto);
    List<AssociadoDTO> buscarTodos();
    AssociadoDTO buscar (Long id);
    AssociadoDTO editar(Long id, AssociadoRequest request);
    void excluir(Long id);
}
