package br.com.vp.voto.api.controller;

import br.com.vp.voto.core.business.SessaoBusiness;
import br.com.vp.voto.model.dto.SessaoComVotosDTO;
import br.com.vp.voto.model.dto.SessaoInDTO;
import br.com.vp.voto.model.dto.SessaoOutDTO;
import io.swagger.v3.oas.annotations.Operation;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/sessao")
public class SessaoController {

    private final SessaoBusiness business;

    @Operation(summary = "Cria uma nova sessão.")
    @PostMapping
    public ResponseEntity<SessaoOutDTO> novo(@RequestBody @Valid SessaoInDTO sessao) {
        return ResponseEntity.status(HttpStatus.CREATED).body(business.novo(sessao));
    }

    @Operation(summary = "Retorna uma lista com todas sessões.")
    @GetMapping
    public ResponseEntity<List<SessaoOutDTO>> buscarTodos() {
        return ResponseEntity.ok(business.buscarTodasSessoes());
    }

    @Operation(summary = "Retorna um objeto Sessão contendo os votos.")
    @GetMapping("/{idSessao}/votos")
    public ResponseEntity<SessaoComVotosDTO> buscarTodosVotosDaSessao(@PathVariable("idSessao") Long idSessao) {
        return ResponseEntity.ok(business.definirVotosDaSessao(idSessao));
    }

    @Operation(summary = "Busca uma sessão pelo id.")
    @GetMapping("/{id}")
    public ResponseEntity<SessaoOutDTO> buscar(@PathVariable("id") Long id) {
        return ResponseEntity.ok(business.buscarSessao(id));
    }
}
