package br.com.vp.voto.api.controller;

import br.com.vp.voto.api.request.PautaRequest;
import br.com.vp.voto.core.service.PautaService;
import br.com.vp.voto.model.dto.PautaDTO;
import io.swagger.v3.oas.annotations.Operation;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@AllArgsConstructor
@RestController
@RequestMapping(path = "/pauta")
public class PautaController {

    private final PautaService service;

    @Operation(summary = "Cria uma nova pauta.")
    @PostMapping(value = "/", produces = APPLICATION_JSON_VALUE, consumes = { APPLICATION_JSON_VALUE })
    public ResponseEntity<PautaDTO> salvar(@RequestBody @Valid PautaRequest pauta) {
        return ResponseEntity.status(HttpStatus.CREATED).body(service.salvar(pauta));
    }

    @Operation(summary = "Retorna uma lista com todas pautas.")
    @GetMapping
    public ResponseEntity<List<PautaDTO>> buscarTodos() {
        List<PautaDTO> pautas = service.buscarTodos();
        return ResponseEntity.ok(pautas);
    }

    @Operation(summary = "Buscar por id.")
    @GetMapping("/{id}")
    public ResponseEntity<PautaDTO> buscar(@PathVariable("id") Long id) {
        return ResponseEntity.ok(service.buscar(id));
    }

    @Operation(summary = "Edita as informações da pauta.")
    @PutMapping("/{id}")
    public ResponseEntity<PautaDTO> editar(@PathVariable("id") Long id, @RequestBody @Valid PautaRequest request) {
        return ResponseEntity.ok(service.editar(id,request));
    }

    @Operation(summary = "Exclui uma pauta")
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void excluir(@PathVariable("id") Long id) {
        service.excluir(id);
    }

}
