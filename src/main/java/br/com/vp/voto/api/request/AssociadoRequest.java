package br.com.vp.voto.api.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.br.CPF;

import javax.validation.constraints.NotBlank;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AssociadoRequest {

    @NotBlank
    private String nome;
    @CPF(message = "CPF inválido!")
    @NotBlank
    private String cpf;
}
