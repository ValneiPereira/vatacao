package br.com.vp.voto.api.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class CpfCadastradoException extends RuntimeException {

    public CpfCadastradoException(String message) {
        super(message);
    }

}
