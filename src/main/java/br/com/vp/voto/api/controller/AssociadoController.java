package br.com.vp.voto.api.controller;

import br.com.vp.voto.api.request.AssociadoRequest;
import br.com.vp.voto.core.service.AssociadoService;
import br.com.vp.voto.model.dto.AssociadoDTO;
import io.swagger.v3.oas.annotations.Operation;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@AllArgsConstructor
@RestController
@RequestMapping(path = "/associado")
public class AssociadoController {

    private final AssociadoService service;

    @Operation(summary = "Cria um novo associado.")
    @PostMapping(value = "/", produces = APPLICATION_JSON_VALUE, consumes = { APPLICATION_JSON_VALUE })
    public ResponseEntity<AssociadoDTO> salvar(@RequestBody @Valid AssociadoRequest associado) {
        return ResponseEntity.status(HttpStatus.CREATED).body(service.salvar(associado));
    }

    @Operation(summary = "Retorna uma lista todos associados.")
    @GetMapping
    public ResponseEntity<List<AssociadoDTO>> buscarTodos() {
        List<AssociadoDTO> associadoDTOS = service.buscarTodos();
        return ResponseEntity.ok().body(associadoDTOS);
    }

    @Operation(summary = "Buscar por id.")
    @GetMapping("/{id}")
    public ResponseEntity<AssociadoDTO> buscar(@PathVariable("id") Long id) {
        return ResponseEntity.ok(service.buscar(id));
    }

    @Operation(summary = "Edita as informações associado.")
    @PutMapping("/{id}")
    public ResponseEntity<AssociadoDTO> editar(@PathVariable("id") Long id, @RequestBody @Valid AssociadoRequest request) {
        return ResponseEntity.ok(service.editar(id,request));
    }

    @Operation(summary = "Exclui um associado.")
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void excluir(@PathVariable("id") Long id) {
        service.excluir(id);
    }
}
