package br.com.vp.voto.api.controller;

import br.com.vp.voto.core.business.VotoBusiness;
import br.com.vp.voto.model.dto.VotoInDTO;
import br.com.vp.voto.model.dto.VotoOutDTO;
import io.swagger.v3.oas.annotations.Operation;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/voto")
public class VotoController {

    private final VotoBusiness business;

    @Operation(summary = "Cria um novo voto.")
    @PostMapping
    public ResponseEntity<VotoOutDTO> novo(@RequestBody @Valid VotoInDTO votoDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(business.novo(votoDto));
    }

    @Operation(summary = "Retorna uma lista de todos os votos.")
    @GetMapping
    public ResponseEntity<List<VotoOutDTO>> buscarTodos() {
        return ResponseEntity.ok(business.buscarTodosVotos());
    }

    @Operation(summary = "Busca um voto pelo id.")
    @GetMapping("/{id}")
    public ResponseEntity<VotoOutDTO> buscar(@PathVariable("id") Long id) {
        return ResponseEntity.ok(business.buscarVoto(id));
    }
}
