package br.com.vp.voto.message;

import br.com.vp.voto.model.dto.ResultadoVotacaoDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service
public class VoteResultsProducer {

    @Value(value = "${vote-results-topic}")
    private String topicNome;

    private final KafkaTemplate<String, ResultadoVotacaoDTO> kafkaTemplate;

    public void sendMessage(Long key, ResultadoVotacaoDTO value) {
        log.info("Mensagem enviada. idSessao: {}. ResultadoVotacao: {}.", key, value);
        kafkaTemplate.send(topicNome, String.valueOf(key), value);
    }
}
