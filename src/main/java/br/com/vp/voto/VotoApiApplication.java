package br.com.vp.voto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableKafka
@EnableScheduling
@SpringBootApplication
public class VotoApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(VotoApiApplication.class, args);
	}

}
